import 'package:flutter/material.dart';

void main() => runApp(App());

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Test',
      theme: ThemeData.dark(),
      home: Counter(),
    );
  }
}

class Counter extends StatefulWidget {
  @override
  CounterState createState() => CounterState();
}

class CounterState extends State<Counter> {
  int value = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Flutter Demo Home Page'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Row(
            children: <Widget>[
              ElevatedButton(
                child: Text(
                  '-',
                  style: TextStyle(fontSize: 50),
                ),
                onPressed: () {
                  setState(() => value--);
                },
              ),
              SizedBox(
                width: 60,
              ),
              Text(
                '$value',
                style: TextStyle(fontSize: 30),
              ),
              SizedBox(
                width: 60,
              ),
              ElevatedButton(
                child: Text(
                  '+',
                  style: TextStyle(fontSize: 50),
                ),
                onPressed: () {
                  setState(() => value++);
                },
              ),
            ],
            mainAxisAlignment: MainAxisAlignment.center,
          ),
          Row(
            children: <Widget>[
              SizedBox(
                height: 200,
              ),
              ElevatedButton(
                child: Text(
                  'Онулирование счетчика',
                  style: TextStyle(fontSize: 30),
                ),
                onPressed: () {
                  setState(() => value = 0);
                },
              ),
            ],
            mainAxisAlignment: MainAxisAlignment.center,
          ),
        ],
      ),
    );
  }
}
